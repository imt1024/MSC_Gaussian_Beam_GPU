Published 08/09/2016

A GPU(CUDA) implementation of a Gaussian Beam Seismic Migration algorithm, chosen
as a masters project for the MSc Computer Science course at the University of Birmingham.

Author: Adam Phillips

Much of this work is addapted from an implementation of this algorithm contained
within the open source project, Seismic Unix (Copyright and disclamer below),
maintained by the Colorado School of Mines.


http://www.cwp.minees.edu/cwpcodes/
Copyright 2007, Colorado School of Mines,
All rights reserved.

    *  Redistributions of source code must retain the above copyright 
       notice, this list of conditions and the following disclaimer.
    *  Redistributions in binary form must reproduce the above 
       copyright notice, this list of conditions and the following 
       disclaimer in the documentation and/or other materials provided 
       with the distribution.
    *  Neither the name of the Colorado School of Mines nor the names of
       its contributors may be used to endorse or promote products 
       derived from this software without specific prior written permission.

Warranty Disclaimer:
THIS SOFTWARE IS PROVIDED BY THE COLORADO SCHOOL OF MINES AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
COLORADO SCHOOL OF MINES OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.

Installation Instructions

1) Install Seismic Unix, source code and installation instructions can be found here : www.cwp.mines.edu/cwpcodes/

2) Install CUDA, instructions provided by Nvidia: https://developer.nvidia.com/cuda-downloads

3) Check the following environment variables are set:
		CWPROOT = Seismic Unix home folder (this should be set during the seismic unix instalation.
		CUDA_PATH = CUDA home folder (this should be set during instalation of CUDA).

4) Download source code from gitlab https://gitlab.com/adamphillips35/MSC_Gaussian_Beam_GPU/repository/archive.zip?ref=master

5) Extract the contents of this zip to your home directory.

6) Navigate to the extracted folder and run make.

Test Data
To test the instaliation:

1) Naviagte to ~/unzipped_folder/test

2) Run ./Migtest_gpu

3) Compare the output of this to the oputput of the cpu version by viewing the relevant seismic files: ./ViewSeismic_cpu and ./ViewSeismic_gpu
