/*****************************************************************************
All methods have been modified from origional code from the Seismic Unix
package. Non gpu version of these following methods were authored by Dave Hale
, Colorado School of Mines, 06/02/89
*****************************************************************************/
#include <cuda.h>
#include "gpu.h"

__device__ void dsinc_gpu (float *a, float x)
/*****************************************************************************
Fills the provided array with sinc(x_mod) = sin(PI*x_mod)/(PI*x_mod)
where x_mod is x mutiplied by the blockIdx.x
******************************************************************************
Input:
*a		array to hold calculated values
x		value at which to evaluate sinc(x)
******************************************************************************
Author:  Adam Phillips
*****************************************************************************/
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	float pix, x_modified = x*tid;

	if (x_modified==0.0) {
		a[tid] = 1.0;
	} else {
		pix = PI*x;
		a[tid] = sin(pix)/pix;
	}
}
__device__ void dsinc_gpu_comp (float *a, float x, float lsinc, float d)
/*****************************************************************************
Fills the provided array with sinc(x_mod) = sin(PI*x_mod)/(PI*x_mod)
where x_mod = x*(lsinc/2-blockIdx.x-1+d)
******************************************************************************
Input:
*a		array to hold calculated values
x		value at which to evaluate sinc(x)
lsinc		length of sinc approximation; lsinc%2==0 and lsinc<=20
d		fractional distance to interpolation point; 0.0<=d<=1.0
******************************************************************************
Author:  Adam Phillips
*****************************************************************************/
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	float pix, x_modified = x*(lsinc/2-tid-1+d);

	if (x_modified==0.0) {
		a[tid] = 1.0;
	} else {
		pix = PI*x;
		a[tid] = sin(pix)/pix;
	}
}
__device__ void stoepd_gpu (int n, float *r, float *g, float *f, float *a)
/*****************************************************************************
Solve a symmetric Toeplitz linear system of equations Rf=g for f
******************************************************************************
Input:
n		dimension of system
r		array[n] of top row of Toeplitz matrix
g		array[n] of right-hand-side column vector

Output:
f		array[n] of solution (left-hand-side) column vector
a		array[n] of solution to Ra=v (Claerbout, FGDP, p. 57)
******************************************************************************
Notes:
An attempt at a CUDA implementation of this can be found in temp.cu, but due
to the nature of this specific section i could not verify weather it is
sutiable for parallelisation.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	int i,j;
	float v,e,c,w,bot;

	if (r[0] == 0.0) return;

	a[0] = 1.0;
	v = r[0];
	f[0] = g[0]/r[0];

	for (j=1; j<n; j++) {
		
		/* solve Ra=v as in Claerbout, FGDP, p. 57 */
		a[j] = 0.0;
		f[j] = 0.0;
		for (i=0,e=0.0; i<j; i++)
			e += a[i]*r[j-i];
		c = e/v;
		v -= c*e;
		for (i=0; i<=j/2; i++) {
			bot = a[j-i]-c*a[i];
			a[i] -= c*a[j-i];
			a[j-i] = bot;
		}

		/* use a and v above to get f[i], i = 0,1,2,...,j */
		for (i=0,w=0.0; i<j; i++)
			w += f[i]*r[j-i];
		c = (w-g[j])/v;
		for (i=0; i<=j; i++)
			f[i] -= c*a[j-i];
	}
}
__device__ void mksinc_gpu (float d, int lsinc, float* sinc)
/*****************************************************************************
Compute least-squares optimal sinc interpolation coefficients.
******************************************************************************
Input:
d		fractional distance to interpolation point; 0.0<=d<=1.0
lsinc		length of sinc approximation; lsinc%2==0 and lsinc<=20

Output:
sinc		array[lsinc] containing interpolation coefficients
******************************************************************************
Author:  Adam Phillips
*****************************************************************************/
{
	int j;
	__shared__ float dev_s[20], dev_a[20], dev_c[20], dev_work[20], fmax;

	/* compute auto-correlation and cross-correlation arrays */
	fmax = 0.066 + 0.265*log((float)lsinc);
	fmax = (fmax < 1.0) ? fmax : 1.0;

	dsinc_gpu(dev_a, fmax);
	dsinc_gpu_comp(dev_c, fmax, lsinc, d);
	__syncthreads();
	/*********************************************************************
	solve symmetric Toeplitz system for the sinc approximation:
	Instead of passing lsinc in (which defiend teh matrix size, the
	each matrix point is done by a single thread. It turns out that this
	May not be possible due to the nature of the method.
	*********************************************************************/
	if (((threadIdx.x + blockIdx.x * blockDim.x) % blockIdx.x) == 0) {
		stoepd_gpu(lsinc, dev_a, dev_c, dev_s, dev_work);
		/* stoepd_gpu(lsinc,a,c,s,work); */
	
		for (j=0; j<lsinc; j++)
			sinc[j] = dev_s[j];
	}
}

/* these are used by both ints8c and ints8r */
#define LTABLE 8
#define NTABLE 513

void intt8c_gpu (int ntable, float table[][8],
	int nxin, float dxin, float fxin, complex yin[],
	complex yinl, complex yinr, int nxout, float xout[], complex yout[])
/*****************************************************************************
Input:
ntable		number of tabulated interpolation operators; ntable>=2
table		array of tabulated 8-point interpolation operators
nxin		number of x values at which y(x) is input
dxin		x sampling interval for input y(x)
fxin		x value of first sample input
yin		array of input y(x) values:  yin[0] = y(fxin), etc.
yinl		value used to extrapolate yin values to left of yin[0]
yinr		value used to extrapolate yin values to right of yin[nxin-1]
nxout		number of x values a which y(x) is output
xout		array of x values at which y(x) is output

Output:
yout		array of output y(x) values:  yout[0] = y(xout[0]), etc.
******************************************************************************
Author: 
origional in ~/seismic_Unix/src/cwp/lib/inttable8.c
*****************************************************************************/
{
	int ioutb,ixout,ixoutn,kyin,ktable,itable,nxinm8;
	float xoutb,xoutf,xouts,xoutn,fntablem1,frac,
		yinlr,yinli,yinrr,yinri,yinir,yinii,sumr,sumi,
		*table00,*pyin,*ptable;
	complex *yin0;

	/* compute constants */
	ioutb = -3-8;
	xoutf = fxin;
	xouts = 1.0/dxin;
	xoutb = 8.0-xoutf*xouts;
	fntablem1 = (float)(ntable-1);
	nxinm8 = nxin-8;
	yin0 = &yin[0];
	table00 = &table[0][0];
	yinlr = yinl.r;  yinli = yinl.i;
	yinrr = yinr.r;  yinri = yinr.i;

	/* loop over output samples */
	for (ixout=0; ixout<nxout; ixout++) {

		/* determine pointers into table and yin */
		xoutn = xoutb+xout[ixout]*xouts;
		ixoutn = (int)xoutn;
		kyin = ioutb+ixoutn;
		pyin = (float*)(yin0+kyin);
		frac = xoutn-(float)ixoutn;
		ktable = frac>=0.0?frac*fntablem1+0.5:(frac+1.0)*fntablem1-0.5;
		ptable = table00+ktable*8;
		
		/* if totally within input array, use fast method */
		if (kyin>=0 && kyin<=nxinm8) {
			yout[ixout].r = 
				pyin[0]*ptable[0]+
				pyin[2]*ptable[1]+
				pyin[4]*ptable[2]+
				pyin[6]*ptable[3]+
				pyin[8]*ptable[4]+
				pyin[10]*ptable[5]+
				pyin[12]*ptable[6]+
				pyin[14]*ptable[7];
			yout[ixout].i = 
				pyin[1]*ptable[0]+
				pyin[3]*ptable[1]+
				pyin[5]*ptable[2]+
				pyin[7]*ptable[3]+
				pyin[9]*ptable[4]+
				pyin[11]*ptable[5]+
				pyin[13]*ptable[6]+
				pyin[15]*ptable[7];
		
		/* else handle end effects with care */
		} else {

			sumr = sumi = 0.0;
			for (itable=0; itable<8; itable++,kyin++) {
				if (kyin<0) {
					yinir = yinlr;
					yinii = yinli;
				} else if (kyin>=nxin) {
					yinir = yinrr;
					yinii = yinri;
				} else {
					yinir = yin[kyin].r;
					yinii = yin[kyin].i;
				}
				sumr += yinir*(*ptable);
				sumi += yinii*(*ptable++);
			}
			yout[ixout].r = sumr;
			yout[ixout].i = sumi;
		}
	}
}

__global__ void mksinc_gpu_controler (int lsin, float* table) {
/*****************************************************************************
Contoler for the mksin part of forming the tables used for interpolation.
******************************************************************************
Input:
lsin		length of the aproximation, fixed at 8

Output:
sinc		updated table with interpolation values
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
	if (blockIdx.x < NTABLE) {
		float frac = (float)blockIdx.x/(float)NTABLE-1;
		mksinc_gpu(frac, LTABLE, &table[blockIdx.x*blockDim.x]);
	}	
}

void ints8c_gpu (int nxin, float dxin, float fxin, complex yin[], 
	complex yinl, complex yinr, int nxout, float xout[], complex yout[])
/*****************************************************************************
Interpolation of a uniformly-sampled complex function y(x) via a
table of 8-coefficient sinc approximations; maximum error for frequiencies
less than 0.6 nyquist is less than one percent.
******************************************************************************
Input:
nxin		number of x values at which y(x) is input
dxin		x sampling interval for input y(x)
fxin		x value of first sample input
yin		array[nxin] of input y(x) values:  yin[0] = y(fxin), etc.
yinl		value used to extrapolate yin values to left of yin[0]
yinr		value used to extrapolate yin values to right of yin[nxin-1]
nxout		number of x values a which y(x) is output
xout		array[nxout] of x values at which y(x) is output

Output:
yout		array[nxout] of output y(x):  yout[0] = y(xout[0]), etc.
******************************************************************************
Author: 
origional in ~/seismic_Unix/src/cwp/lib/intsinc8.c
*****************************************************************************/
{
	static float table[NTABLE][LTABLE];
	float* dev_table;
	static bool tabled=false;

	/* tabulate sinc interpolation coefficients if not already tabulated */
	if (!tabled) {
		cudaMalloc((void**) &dev_table, sizeof(table));
		/*
		for (jtable=1; jtable<NTABLE-1; jtable++) {
			frac = (float)jtable/(float)(NTABLE-1);
			mksinc_gpu(frac,LTABLE,&table[jtable][0]);
		}*/
		mksinc_gpu_controler<<<NTABLE,LTABLE>>>(LTABLE, dev_table);
		cudaMemcpy(table, dev_table, sizeof(table), cudaMemcpyDeviceToHost);
		for (int jtable=0; jtable<LTABLE; jtable++) {
			table[0][jtable] = 0.0;
			table[NTABLE-1][jtable] = 0.0;
		}
		table[0][LTABLE/2-1] = 1.0;
		table[NTABLE-1][LTABLE/2] = 1.0;
		tabled = true;
		cudaFree(dev_table);
	}

	/* interpolate using tabulated coefficients */
	intt8c_gpu(NTABLE,table,nxin,dxin,fxin,yin,yinl,yinr,nxout,xout,yout);
}
