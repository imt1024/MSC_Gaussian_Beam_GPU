/****************************************************************************
Lots of headers adapted from cwp.h, ogigional at seismic_unix/src/cwp/include
****************************************************************************/
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <limits.h>
#include <float.h>
#include <stdarg.h>

#include <fcntl.h>      /* non-ANSI */
#include <unistd.h>     /* non-ANSI */
#include <sys/types.h>  /* non-ANSI */

/* TYPEDEFS */
typedef enum {cwp_false, cwp_true} cwp_Bool;
typedef char *cwp_String;

typedef enum {BADFILETYPE = -1,
        TTY, DISK, DIRECTORY, TAPE, PIPE, FIFO, SOCKET, SYMLINK} FileType;
        
//#if defined(CRAY) || defined(OVERRIDE_CWP_COMPLEX)
typedef struct _complexStruct { /* complex number */
	float r,i;
}  cwp_complex;
typedef struct _dcomplexStruct { /* double-precision complex number */
	double r,i;
}  cwp_dcomplex;
//#endif
//#ifndef __cplusplus /* if not C++, define the C struct complex */
//#ifndef complex
//typedef struct _complexStruct { /* complex number */
//	float r,i;
//} complex;
//#endif/* complex */

//#else /* if C++, define the C++ class complex */
#include "Complex.h"
//#endif
#define complex cwp_complex
#define dcomplex cwp_dcomplex
#define cwp_cmplx complex
#define cadd cwp_cadd
#define csub cwp_csub
#define cmul cwp_cmul
#define cdiv cwp_cdiv
#define rcabs cwp_rcabs
#define cmplx cwp_cmplx
#define conjg cwp_conjg
#define cneg cwp_cneg
#define cinv cwp_cinv
#define cwp_csqrt cwp_csqrt
#define cwp_cexp cwp_cexp
#define crmul cwp_crmul
#define cipow cwp_cipow
#define crpow cwp_crpow
#define rcpow cwp_rcpow
#define ccpow cwp_ccpow
#define cwp_ccos cwp_ccos
#define cwp_csin cwp_csin
#define cwp_ccosh cwp_ccosh
#define cwp_csinh cwp_csinh
#define cwp_cexp1 cwp_cexp1
#define cwp_clog cwp_clog

#ifdef CWP_BIG_ENDIAN
#define CWPENDIAN 1
#endif
#ifdef CWP_LITTLE_ENDIAN
#define CWPENDIAN 0
#endif


#ifndef NULL
#define NULL	((void *)0)
#endif
#ifndef EXIT_FAILURE
#define EXIT_FAILURE (1)
#endif
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS (0)
#endif
#ifndef SEEK_SET
#define SEEK_SET (0)
#endif
#ifndef SEEK_CUR
#define SEEK_CUR (1)
#endif
#ifndef SEEK_END
#define SEEK_END (2)
#endif
#ifndef PI
#define PI (3.141592653589793)
#endif
#ifndef D_PI 
#define D_PI (double) (3.1415926535897932385)
#endif
#ifndef GOLDEN_RATIO 
#define GOLDEN_RATIO (1.618034)   /* the golden ratio */
#endif
#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef YES
#define YES (1)
#endif
#ifndef NO
#define NO (0)
#endif
#ifndef SGN
#define SGN(x) ((x) < 0 ? -1.0 : 1.0)
#endif
#ifndef ABS
#define ABS(x) ((x) < 0 ? -(x) : (x))
#endif
#ifndef MAX
#define	MAX(x,y) ((x) > (y) ? (x) : (y))
#endif
#ifndef MIN
#define	MIN(x,y) ((x) < (y) ? (x) : (y))
#endif
#define NINT(x) ((int)((x)>0.0?(x)+0.5:(x)-0.5))
#define CLOSETO(x, y) ((ABS((x) - (y)) <= FLT_EPSILON*ABS(y))?cwp_true:cwp_false)
#define ISODD(n) ((n) & 01)
#define ISIZE sizeof(int)
#define FSIZE sizeof(float)
#define CSIZE sizeof(complex)
#define DSIZE sizeof(double)
#define	STREQ(s,t) (strcmp(s,t) == 0)
#define	STRLT(s,t) (strcmp(s,t) < 0)
#define	STRGT(s,t) (strcmp(s,t) > 0)
#define	DIM(a) (sizeof(a)/sizeof(a[0]))
#define SQR(x) ((x))*((x))

/* GLOBAL DECLARATIONS */
extern int xargc; extern char **xargv;

//Test method for linking
int vector_add_gpu_test(int o);

/* allocate and free multi-dimensional arrays */
void *alloc1 (size_t n1, size_t size);
void *realloc1 (void *v, size_t n1, size_t size);
void **alloc2 (size_t n1, size_t n2, size_t size);
void ***alloc3 (size_t n1, size_t n2, size_t n3, size_t size);
void ****alloc4 (size_t n1, size_t n2, size_t n3, size_t n4, size_t size);
void *****alloc5 (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t size);
void ******alloc6 (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t n6, 
                   size_t size);

void free1 (void *p);
void free2 (void **p);
void free3 (void ***p);
void free4 (void ****p);
void free5 (void *****p);
void free6 (void ******p);
int *alloc1int (size_t n1);
int *realloc1int (int *v, size_t n1);
int **alloc2int (size_t n1, size_t n2);
int ***alloc3int (size_t n1, size_t n2, size_t n3);
float *alloc1float (size_t n1);
float *realloc1float (float *v, size_t n1);
float **alloc2float (size_t n1, size_t n2);
float ***alloc3float (size_t n1, size_t n2, size_t n3);

float ****alloc4float (size_t n1, size_t n2, size_t n3, size_t n4);
void free4float (float ****p);
float *****alloc5float (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5);
void free5float (float *****p);
float ******alloc6float (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t n6);
void free6float (float ******p);
int ****alloc4int (size_t n1, size_t n2, size_t n3, size_t n4);
void free4int (int ****p);
int *****alloc5int (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5);
void free5int (int *****p);
unsigned short ******alloc6ushort(size_t n1,size_t n2,size_t n3,size_t n4,
        size_t n5, size_t n6);
unsigned char *****alloc5uchar(size_t n1,size_t n2,size_t n3,size_t n4,
        size_t n5);
void free5uchar(unsigned char *****p);
unsigned short *****alloc5ushort(size_t n1,size_t n2,size_t n3,size_t n4,
        size_t n5);
void free5ushort(unsigned short *****p);
unsigned char ******alloc6uchar(size_t n1,size_t n2,size_t n3,size_t n4,
        size_t n5, size_t n6);
void free6uchar(unsigned char ******p);
unsigned short ******alloc6ushort(size_t n1,size_t n2,size_t n3,size_t n4,
        size_t n5, size_t n6);
void free6ushort(unsigned short ******p);

double *alloc1double (size_t n1);
double *realloc1double (double *v, size_t n1);
double **alloc2double (size_t n1, size_t n2);
double ***alloc3double (size_t n1, size_t n2, size_t n3);
complex *alloc1complex (size_t n1);
complex *realloc1complex (complex *v, size_t n1);
complex **alloc2complex (size_t n1, size_t n2);
complex ***alloc3complex (size_t n1, size_t n2, size_t n3);

dcomplex *alloc1dcomplex (size_t n1);
dcomplex *realloc1dcomplex (dcomplex *v, size_t n1);
dcomplex **alloc2dcomplex (size_t n1, size_t n2);
dcomplex ***alloc3dcomplex (size_t n1, size_t n2, size_t n3);

void free1int (int *p);
void free2int (int **p);
void free3int (int ***p);
void free1float (float *p);
void free2float (float **p);
void free3float (float ***p);

void free1double (double *p);
void free2double (double **p);
void free3double (double ***p);
void free1complex (complex *p);
void free2complex (complex **p);
void free3complex (complex ***p);

void free1dcomplex (dcomplex *p);
void free2dcomplex (dcomplex **p);
void free3dcomplex (dcomplex ***p);

/* contained within alloc, warn */
void warn(char *fmt, ...);

#ifndef __cplusplus
complex cadd (complex a, complex b);
complex csub (complex a, complex b);
complex cmul (complex a, complex b);
complex cdiv (complex a, complex b);
float rcabs (complex z);
complex cmplx (float re, float im);
complex conjg (complex z);
complex cneg (complex z);
complex cinv (complex z);
complex cwp_csqrt (complex z);
complex cwp_cexp (complex z);
complex crmul (complex a, float x);

/* complex functions */
complex cipow(complex a, int p);
complex crpow(complex a, float p);
complex rcpow(float a, complex p);
complex ccpow (complex a, complex p);
complex cwp_ccos(complex a);
complex cwp_csin(complex a);
complex cwp_ccosh(complex a);
complex cwp_csinh(complex a);
complex cwp_cexp1(complex a);
complex cwp_clog(complex a);

/* double complex */
dcomplex dcadd (dcomplex a, dcomplex b);
dcomplex dcsub (dcomplex a, dcomplex b);
dcomplex dcmul (dcomplex a, dcomplex b);
dcomplex dcdiv (dcomplex a, dcomplex b);
double drcabs (dcomplex z);
dcomplex dcmplx (double re, double im);
dcomplex dconjg (dcomplex z);
dcomplex dcneg (dcomplex z);
dcomplex dcinv (dcomplex z);
dcomplex dcsqrt (dcomplex z);
dcomplex dcexp (dcomplex z);
dcomplex dcrmul (dcomplex a, double x);

/* double complex functions */
dcomplex dcipow(dcomplex a, int p);
dcomplex dcrpow(dcomplex a, float p);
dcomplex rdcpow(float a, dcomplex p);
dcomplex dcdcpow(dcomplex a, dcomplex p);
dcomplex dccos(dcomplex a);
dcomplex dcsin(dcomplex a);
dcomplex dccosh(dcomplex a);
dcomplex dcsinh(dcomplex a);
dcomplex dcexp1(dcomplex a);
dcomplex dclog(dcomplex a);
#endif
#ifdef COMP_GPU
/* ints8c */
__device__ void dsinc_gpu (float *a, float x);
__device__ void dsinc_gpu_comp (float *a, float x, float lsinc, float d);
__device__ void stoepd_gpu (int n, float *r, float *g, float *f, float *a);
__device__ void mksinc_gpu (float d, int lsinc, float* sinc);
__global__ void mksinc_gpu_controler (int lsin, float* table);
void intt8c_gpu (int ntable, float table[][8], int nxin, float dxin,
	float fxin, complex yin[], complex yinl, complex yinr,
	int nxout, float xout[], complex yout[]);
void ints8c_gpu (int nxin, float dxin, float fxin, complex yin[],
	complex yinl, complex yinr, int nxout, float xout[],
	complex yout[]);
#endif
#ifdef COMP_CPU
double dsinc (double x);
void stoepd (int n, double r[], double g[], double f[], double a[]);
void mksinc (float d, int lsinc, float sinc[]);
void intt8c (int ntable, float table[][8],
	int nxin, float dxin, float fxin, complex yin[],
	complex yinl, complex yinr, int nxout, float xout[], complex yout[]);
void ints8c (int nxin, float dxin, float fxin, complex yin[], 
	complex yinl, complex yinr, int nxout, float xout[], complex yout[]);
#endif

/* pffft */
int npfa_gpu (int nmin);
int npfao_gpu (int nmin, int nmax);
int npfar_gpu (int nmin);
int npfaro_gpu (int nmin, int nmax);
void pfacr_gpu (int isign, int n, complex cz[], float rz[]);
void pfarc_gpu (int isign, int n, float rz[], complex cz[]);
void pfacc_gpu (int isign, int n, complex cz[]);

/* rng */
float franuni (void);
