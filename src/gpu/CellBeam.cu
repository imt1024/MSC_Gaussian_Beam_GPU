#include "gpu.h"

extern float *global_dev_g;
/*****************************************************************************
Origional Method from cpu version

static void cellBeam (Cells *cells, int jx, int jz)
******************************************************************************
Accumulate Gaussian beam for one cell.
******************************************************************************
Input:
cells		pointer to cells
jx		x index of the cell in which to accumulate beam
jz		z index of the cell in which to accumulate beam
******************************************************************************
{
	int lx=cells->lx,lz=cells->lz,nx=cells->nx,nz=cells->nz;
	float **g=cells->g;
	Cell **cell=cells->cell;
	BeamData *bd=cells->bd;
	int ntr=bd->ntr,nti=bd->nti;
	float dtr=bd->dtr,ftr=bd->ftr,dti=bd->dti,fti=bd->fti;
	complex **cf=bd->cf;
	int kxlo,kxhi,kzlo,kzhi,kx,kz,itr,iti;
	float odtr,odti,t00r,t01r,t10r,t11r,t00i,t01i,t10i,t11i,
		a00r,a01r,a10r,a11r,a00i,a01i,a10i,a11i,
		tx0r,tx1r,tx0i,tx1i,ax0r,ax1r,ax0i,ax1i,
		txzr,txzi,axzr,axzi,
		dtx0r,dtx0i,dtx1r,dtx1i,dax0r,dax0i,dax1r,dax1i,
		dtxzr,dtxzi,daxzr,daxzi,xdelta,zdelta,
		trn,tin,trfrac,mtrfrac,tifrac,mtifrac,
		cf0r,cf0i,cf1r,cf1i,cfr,cfi;
	complex *cf0,*cf1;

	//inverse of time sampling intervals
	odtr = 1.0/dtr;
	odti = 1.0/dti;

	// complex time and amplitude for each corner
	t00r = cell[jx][jz].tr;
	t01r = cell[jx][jz+1].tr;
	t10r = cell[jx+1][jz].tr;
	t11r = cell[jx+1][jz+1].tr;
	t00i = cell[jx][jz].ti;
	t01i = cell[jx][jz+1].ti;
	t10i = cell[jx+1][jz].ti;
	t11i = cell[jx+1][jz+1].ti;
	a00r = cell[jx][jz].ar;
	a01r = cell[jx][jz+1].ar;
	a10r = cell[jx+1][jz].ar;
	a11r = cell[jx+1][jz+1].ar;
	a00i = cell[jx][jz].ai;
	a01i = cell[jx][jz+1].ai;
	a10i = cell[jx+1][jz].ai;
	a11i = cell[jx+1][jz+1].ai;

	// x and z samples for cell
	kxlo = jx*lx;
	kxhi = kxlo+lx;
	if (kxhi>nx) kxhi = nx;
	kzlo = jz*lz;
	kzhi = kzlo+lz;
	if (kzhi>nz) kzhi = nz;

	// fractional increments for linear interpolation
	xdelta = 1.0/lx;
	zdelta = 1.0/lz;

	// increments for times and amplitudes at top and bottom of cell
	dtx0r = (t10r-t00r)*xdelta;
	dtx1r = (t11r-t01r)*xdelta;
	dtx0i = (t10i-t00i)*xdelta;
	dtx1i = (t11i-t01i)*xdelta;
	dax0r = (a10r-a00r)*xdelta;
	dax1r = (a11r-a01r)*xdelta;
	dax0i = (a10i-a00i)*xdelta;
	dax1i = (a11i-a01i)*xdelta;
	
	// times and amplitudes at top-left and bottom-left of cell
	tx0r = t00r;
	tx1r = t01r;
	tx0i = t00i;
	tx1i = t01i;
	ax0r = a00r;
	ax1r = a01r;
	ax0i = a00i;
	ax1i = a01i;

	// loop over x samples
	for (kx=kxlo; kx<kxhi; ++kx) {
		// increments for time and amplitude
		dtxzr = (tx1r-tx0r)*zdelta;
		dtxzi = (tx1i-tx0i)*zdelta;
		daxzr = (ax1r-ax0r)*zdelta;
		daxzi = (ax1i-ax0i)*zdelta;

		// time and amplitude at top of cell
		txzr = tx0r;
		txzi = tx0i;
		axzr = ax0r;
		axzi = ax0i;

		// loop over z samples
		for (kz=kzlo; kz<kzhi; ++kz) {
			// index of imaginary time
			iti = tin = (txzi-fti)*odti;
			if (iti<0 || iti>=nti-1) continue;

			// pointers to left and right imaginary time samples
			cf0 = cf[iti];
			cf1 = cf[iti+1];

			// imaginary time linear interpolation coefficients
			tifrac = tin-iti;
			mtifrac = 1.0-tifrac;

			// index of real time
			itr = trn = (txzr-ftr)*odtr;
			if (itr<0 || itr>=ntr-1) continue;

			// real time linear interpolation coefficients
			trfrac = trn-itr;
			mtrfrac = 1.0-trfrac;

			// real and imaginary parts of complex beam data
			cf0r = mtrfrac*cf0[itr].r+trfrac*cf0[itr+1].r;
			cf1r = mtrfrac*cf1[itr].r+trfrac*cf1[itr+1].r;
			cfr = mtifrac*cf0r+tifrac*cf1r;
			cf0i = mtrfrac*cf0[itr].i+trfrac*cf0[itr+1].i;
			cf1i = mtrfrac*cf1[itr].i+trfrac*cf1[itr+1].i;
			cfi = mtifrac*cf0i+tifrac*cf1i;

			// accumulate beam
			g[kx][kz] += axzr*cfr-axzi*cfi;

			// increment time and amplitude
			txzr += dtxzr;
			txzi += dtxzi;
			axzr += daxzr;
			axzi += daxzi;
		}

		// increment times and amplitudes at top and bottom of cell
		tx0r += dtx0r;
		tx1r += dtx1r;
		tx0i += dtx0i;
		tx1i += dtx1i;
		ax0r += dax0r;
		ax1r += dax1r;
		ax0i += dax0i;
		ax1i += dax1i;
	}
}
*****************************************************************************/

/****************************Used Internally*********************************/
__global__ void cellBeam_dev(Cells *cells, int jx, int jz);
__global__ void cellBeam_dev3(int jx, int jz);
static void cellBeam1(Cells *cells, int jx, int jz);
static void cellBeam2(Cells *cells, Cells *dev_cells, int jx, int jz);
static void cellBeam3(int jx, int jz);
/****************************************************************************/

void cellBeam (Cells *cells, Cells *dev_cells, int jx, int jz)
/*****************************************************************************
Accumulate Gaussian beam for one cell.
******************************************************************************
Input:
cells		pointer to cells
jx		x index of the cell in which to accumulate beam
jz		z index of the cell in which to accumulate beam

Note: This method is called from sum_main_gpu.cu and can be adjusted to see
relative changes in different implementations; 1,2...
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Implimentation 1
	//cellBeam1(cells, jx, jz);
	
	//Implimentation 2
	//cellBeam2(cells, dev_cells, jx, jz);
	
	//Implementation 3
	cellBeam2(cells, dev_cells, jx, jz);
}

static void cellBeam1(Cells *cells, int jx, int jz)
/*****************************************************************************
A simple implimentation where each time the method is called a memory is
copied and a kenrel is launched, then after the kernel execution memory is
copyed back and freed.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Allocate memory on gpu for cells struct
	Cells* dev_cells = allocGpuCells();
	
	//Copy cells struct over to gpu
	copyCellsHostToDevice(cells, dev_cells);
	
	//Check the contents of the cells struct
	//checkCells(cells, dev_cells);

	cellBeam_dev<<<cells->lx,cells->lz>>>(dev_cells, jx, jz);
	gpuErrorCheck(cudaPeekAtLastError());	
	
	//Copy cells struct back to Host memory
	copyCellsDeviceToHost(cells);
	
	//Free Device Memory
	freeGpuCells(cells, dev_cells);
}


static void cellBeam2 (Cells *cells, Cells *dev_cells, int jx, int jz)
/*****************************************************************************
An attempted improvment upon Implimentation1, for each beam data is only
copied once, then back when each beams calculations have finished. Aim's to
reduce the large overhead of copying data.
Uses *dev_cells which is replaced for each beam.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Call cellBeam_dev using cells stored in shared memory.
	cellBeam_dev<<<cells->lx,cells->lz>>>(dev_cells, jx, jz);
	gpuErrorCheck(cudaPeekAtLastError());
	
	//checkCells(cells, dev_cells);
}

void cellBeam2CopyFreeSharedCells(Cells *cells, Cells *dev_cells)
/*****************************************************************************
This method copys the resulting stored data from teh device back to the host
and then frees the device memory.

Note: For this method to work it must be called after each of the Beams has
finished this is done within the Accumulate Beam method (accBeam) after the
call to Accumulate Cells (accCell).
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	if (dev_cells == NULL) return;
	//Copy results to host
	copyCellsDeviceToHost(cells);
	//checkCells(cells, dev_cells);
	
	//Free Device Memory
	freeGpuCells(cells, dev_cells);
}

__global__ void cellBeam_dev(Cells *cells, int jx, int jz)
/*****************************************************************************
Performs beam accumulation on one point within a cell defined by its internal
cell x, y cordinate. each thread will perform for one point.
******************************************************************************
Input:
cells		pointer to cells on device
jx		x index of the cell in which to accumulate beam
jz		z index of the cell in which to accumulate beam
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Setup of initial parameters with ** changed to *
	int lx=cells->lx,lz=cells->lz,nx=cells->nx,nz=cells->nz,mz=cells->mz;
	float *g=cells->dev_g;
	Cell *cell=cells->dev_cell;
	BeamData *bd=cells->dev_bd;
	int ntr=bd->ntr,nti=bd->nti;
	float dtr=bd->dtr,ftr=bd->ftr,dti=bd->dti,fti=bd->fti;
	complex *cf=bd->dev_cf;
	int kxlo,kxhi,kzlo,kzhi,kx,kz,itr,iti;
	float odtr,odti,t00r,t01r,t10r,t11r,t00i,t01i,t10i,t11i,
		a00r,a01r,a10r,a11r,a00i,a01i,a10i,a11i,
		tx0r,tx1r,tx0i,tx1i,ax0r,ax1r,ax0i,ax1i,
		txzr,txzi,axzr,axzi,
		dtx0r,dtx0i,dtx1r,dtx1i,dax0r,dax0i,dax1r,dax1i,
		dtxzr,dtxzi,daxzr,daxzi,xdelta,zdelta,
		trn,tin,trfrac,mtrfrac,tifrac,mtifrac,
		cf0r,cf0i,cf1r,cf1i,cfr,cfi;
	complex *cf0,*cf1;
	int xid, zid;

	//inverse of time sampling intervals
	odtr = 1.0/dtr;
	odti = 1.0/dti;

	// complex time and amplitude for each corner
	t00r = cell[jx  * mz + jz].tr;
	t01r = cell[jx * mz + (jz+1)].tr;
	t10r = cell[(jx+1)* mz + jz ].tr;
	t11r = cell[(jx+1) * mz + (jz+1)].tr;
	t00i = cell[jx * mz + jz].ti;
	t01i = cell[jx * mz + (jz+1)].ti;
	t10i = cell[(jx+1) * mz + jz].ti;
	t11i = cell[(jx+1) * mz + (jz+1)].ti;
	a00r = cell[jx * mz + jz].ar;
	a01r = cell[jx * mz + (jz+1)].ar;
	a10r = cell[(jx+1) * mz + (jz)].ar;
	a11r = cell[(jx+1) * mz + (jz+1)].ar;
	a00i = cell[jx * mz + jz].ai;
	a01i = cell[jx * mz + (jz+1)].ai;
	a10i = cell[(jx+1) * mz + jz].ai;
	a11i = cell[(jx+1) * mz + (jz+1)].ai;

	// x and z samples for cell
	kxlo = jx*lx;
	kxhi = kxlo+lx;
	if (kxhi>nx) kxhi = nx;
	kzlo = jz*lz;
	kzhi = kzlo+lz;
	if (kzhi>nz) kzhi = nz;

	// fractional increments for linear interpolation
	xdelta = 1.0/lx;
	zdelta = 1.0/lz;

	// increments for times and amplitudes at top and bottom of cell
	dtx0r = (t10r-t00r)*xdelta;
	dtx1r = (t11r-t01r)*xdelta;
	dtx0i = (t10i-t00i)*xdelta;
	dtx1i = (t11i-t01i)*xdelta;
	dax0r = (a10r-a00r)*xdelta;
	dax1r = (a11r-a01r)*xdelta;
	dax0i = (a10i-a00i)*xdelta;
	dax1i = (a11i-a01i)*xdelta;
	
	// times and amplitudes at top-left and bottom-left of cell
	tx0r = t00r;
	tx1r = t01r;
	tx0i = t00i;
	tx1i = t01i;
	ax0r = a00r;
	ax1r = a01r;
	ax0i = a00i;
	ax1i = a01i;
	
	//Check bounds of block/thread id are within bounds
	xid = blockIdx.x;
	zid = threadIdx.x;
	if(xid < lx && zid < lz) {
		//Output array id, now only one after conversion
		kx = kxlo + xid;
		kz = kzlo + zid;
		
		// increment times and amplitudes at top and bottom of cell
		//tx0r += dtx0r;
		tx0r = tx0r + (xid * dtx0r);
		//tx1r += dtx1r;
		tx1r = tx1r + (xid * dtx1r);
		//tx0i += dtx0i;
		tx0i = tx0i + (xid * dtx0i);
		//tx1i += dtx1i;
		tx1i = tx1i + (xid * dtx1i);
		//ax0r += dax0r;
		ax0r = ax0r + (xid * dax0r);
		//ax1r += dax1r;
		ax1r = ax1r + (xid * dax1r);
		//ax0i += dax0i;
		ax0i = ax0i + (xid * dax0i);
		//ax1i += dax1i;
		ax1i = ax1i + (xid * dax1i);
		
		// increments for time and amplitude
		dtxzr = (tx1r-tx0r)*zdelta;
		dtxzi = (tx1i-tx0i)*zdelta;
		daxzr = (ax1r-ax0r)*zdelta;
		daxzi = (ax1i-ax0i)*zdelta;
		
		// time and amplitude at top of cell
		//txzr = tx0r;
		txzr = tx0r + (zid * dtxzr);
		//txzi = tx0i;
		txzi = tx0i + (zid * dtxzi);
		//axzr = ax0r;
		axzr = ax0r + (zid * daxzr);
		//axzi = ax0i;
		axzi = ax0i + (zid * daxzi);
		
		//Same core computation now done on each thread
		// index of imaginary time
		iti = tin = (txzi-fti)*odti;
		if (iti<0 || iti>=nti-1) return;

		// pointers to left and right imaginary time samples
		cf0 = &cf[iti * nti];
		cf1 = &cf[iti+1 * nti];

		// imaginary time linear interpolation coefficients
		tifrac = tin-iti;
		mtifrac = 1.0-tifrac;

		// index of real time
		itr = trn = (txzr-ftr)*odtr;
		if (itr<0 || itr>=ntr-1) return;

		// real time linear interpolation coefficients
		trfrac = trn-itr;
		mtrfrac = 1.0-trfrac;

		// real and imaginary parts of complex beam data
		cf0r = mtrfrac*cf0[itr].r+trfrac*cf0[itr+1].r;
		cf1r = mtrfrac*cf1[itr].r+trfrac*cf1[itr+1].r;
		cfr = mtifrac*cf0r+tifrac*cf1r;
		cf0i = mtrfrac*cf0[itr].i+trfrac*cf0[itr+1].i;
		cf1i = mtrfrac*cf1[itr].i+trfrac*cf1[itr+1].i;
		cfi = mtifrac*cf0i+tifrac*cf1i;

		// accumulate beam
		g[kz + kx * nz] += axzr*cfr-axzi*cfi;
	}
}


