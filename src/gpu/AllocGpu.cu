#include "gpu.h"

extern float *global_dev_g;
__global__ static void devCheckCells(Cells *cells,
 int *nt, int *lx, int *mx, int *nz, int *live, int *dead,
 float *dt, float *ft, float *dx, float *fx, float *dz, float *fz,
  float *wmin, float *lmin);

Cells* allocGpuCells ()
/*****************************************************************************
Allocates a Cells struct on the gpu and returns a pointer to said struct.

Struct:
struct CellsStruct {
	int nt;		// number of time samples
	float dt;	// time sampling interval
	float ft;	// first time sample
	int lx;		// number of x samples per cell
	int mx;		// number of x cells
	int nx;		// number of x samples
	float dx;	// x sampling interval
	float fx;	// first x sample
	int lz;		// number of z samples per cell
	int mz;		// number of z cells
	int nz;		// number of z samples
	float dz;	// z sampling interval
	float fz;	// first z sample
	int live;	// random number used to denote a live cell
	int dead;	// random number used to denote a dead cell
	float wmin;	// minimum (reference) frequency
	float lmin;	// minimum beamwidth for frequency wmin
	Cell **cell;	// cell array[mx][mz]
	Ray *ray;	// ray
	BeamData *bd;	// complex beam data as a function of complex time
	float **g;	// array[nx][nz] containing g(x,z)
} Cells;
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	Cells* dev_cells;
	gpuErrorCheck(cudaMalloc((void**) &dev_cells, sizeof(Cells)));
	return dev_cells;
}

void allocGonDevice (int nx, int nz)
/*****************************************************************************
Allocate space for the g output array on the device, set global_dev_g as
pointer to device.

int nx;		number of x samples
int nz;		number of z samples
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	gpuErrorCheck(cudaMalloc((void**) &global_dev_g,
		nx * nz * sizeof(float)));
}

void freeGpuCells (Cells *cells, Cells *cellsGpu)
/*****************************************************************************
Frees memory on the gpu used by the Cells struct, will free all memory even
those that are beign pointed to. Does not free memory for Rays.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	Cell *dev_cell=cells->dev_cell;
	BeamData *dev_bd=cells->dev_bd, *bd = cells->bd;
	complex *dev_cf=bd->dev_cf;
	float *dev_g=cells->dev_g;
		
	//free memory pointed to be dev poniters
	gpuErrorCheck(cudaFree(dev_g));
	cells->dev_g=NULL;
	gpuErrorCheck(cudaFree(dev_cf));
	bd->dev_cf=NULL;
	gpuErrorCheck(cudaFree(dev_bd));
	cells->dev_bd=NULL;
	gpuErrorCheck(cudaFree(dev_cell));
	cells->dev_cell=NULL;
	if (cellsGpu == NULL) return;
	gpuErrorCheck(cudaFree(cellsGpu));
}

void freeGOnDevice ()
/*****************************************************************************
Frees the output array from the device. Assumes gloabl_dev_g hols a device
pointer.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	if (global_dev_g == NULL) {
		warn("Gloabl_dev_g value is NULL!");
		return;
	}
	
	gpuErrorCheck(cudaFree(global_dev_g));
}

void freeGpuCellsNoG (Cells *cells, Cells *cellsGpu)
/*****************************************************************************
Free all memory on the GPU used by cells, but does not free data asscoiated
with the g output data.
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	Cell *dev_cell=cells->dev_cell;
	BeamData *dev_bd=cells->dev_bd;
	complex *dev_cf=cells->bd->dev_cf;
		
	//free memory pointed to be dev poniters
	gpuErrorCheck(cudaFree(dev_cf));
	gpuErrorCheck(cudaFree(dev_bd));
	gpuErrorCheck(cudaFree(dev_cell));
	gpuErrorCheck(cudaFree(cellsGpu));
}

void copyCellsHostToDevice (Cells *cells, Cells *dev_cells)
/*****************************************************************************
Copys the Cells struct and all its containing data to the device.
Allocates space on the device for data pointed to by the cells
Also copys the data pointed to by **cell, *bd and **g.

Note: The *ray data is not copied as it is not yet used by any written method
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Setup local variabls for internal contents to be copied
	Cell *dev_cell;
	BeamData *dev_bd, *bd = cells->bd;
	complex *dev_cf;
	float *dev_g;
	int mx, mz, nx, nz, ntr, nti; //used for size of elements
	
	mx = cells->mx;
	mz = cells->mz;
	nx = cells->nx;
	nz = cells->nz;
	ntr = cells->bd->ntr;
	nti = cells->bd->nti;
	
	//Allocate and copy device memory for Cell array.
	gpuErrorCheck(cudaMalloc((void**)&dev_cell, mx * mz * sizeof(Cell)));
	gpuErrorCheck(cudaMemcpy(dev_cell, cells->cell[0],mx * mz * sizeof(Cell),
		cudaMemcpyHostToDevice));
	cells->dev_cell=dev_cell;
	
	//Allocate and copy complex[] from bd
	gpuErrorCheck(cudaMalloc((void**)&dev_cf, nti*ntr*sizeof(complex)));
	gpuErrorCheck(cudaMemcpy(dev_cf, bd->cf[0], nti*ntr*sizeof(complex),
		cudaMemcpyHostToDevice));
	bd->dev_cf=dev_cf;
	
	//Allocate and copy device memory for BeamData
	gpuErrorCheck(cudaMalloc((void**)&dev_bd, sizeof(BeamData)));
	gpuErrorCheck(cudaMemcpy(dev_bd, cells->bd, sizeof(BeamData), 
		cudaMemcpyHostToDevice));
	cells->dev_bd=dev_bd;
	
	//Allocate and copy the output g[][]
	gpuErrorCheck(cudaMalloc((void**)&dev_g, nx * nz * sizeof(float)));
	gpuErrorCheck(cudaMemcpy(dev_g, cells->g[0], nx * nz * sizeof(float),
		cudaMemcpyHostToDevice));
	cells->dev_g=dev_g;
	
	//Copy Cells over to Device
	gpuErrorCheck(cudaMemcpy(dev_cells, cells, sizeof(Cells),
		cudaMemcpyHostToDevice));
}

void copyCellsHostToDeviceNoG (Cells *cells, Cells *dev_cells)
/*****************************************************************************
Copys the Cells struct and all its containing data to the device.
Allocates space on the device for data pointed to by the cells
Also copys the data pointed to by **cell and *bd.
Does not copy over the g* output data.

Note: The *ray data is not copied as it is not yet used by any written method
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Setup local variabls for internal contents to be copied
	Cell *dev_cell;
	BeamData *dev_bd, *bd = cells->bd;
	complex *dev_cf;
	int mx, mz, ntr, nti; //used for size of elements
	
	mx = cells->mx;
	mz = cells->mz;
	ntr = cells->bd->ntr;
	nti = cells->bd->nti;
	
	//Allocate and copy device memory for Cell array.
	gpuErrorCheck(cudaMalloc((void**)&dev_cell, mx * mz * sizeof(Cell)));
	gpuErrorCheck(cudaMemcpy(dev_cell, cells->cell[0],mx * mz * sizeof(Cell),
		cudaMemcpyHostToDevice));
	cells->dev_cell=dev_cell;
	
	//Allocate and copy complex[] from bd
	gpuErrorCheck(cudaMalloc((void**)&dev_cf, nti*ntr*sizeof(complex)));
	gpuErrorCheck(cudaMemcpy(dev_cf, bd->cf[0], nti*ntr*sizeof(complex),
		cudaMemcpyHostToDevice));
	bd->dev_cf=dev_cf;
	
	//Allocate and copy device memory for BeamData
	gpuErrorCheck(cudaMalloc((void**)&dev_bd, sizeof(BeamData)));
	gpuErrorCheck(cudaMemcpy(dev_bd, cells->bd, sizeof(BeamData), 
		cudaMemcpyHostToDevice));
	cells->dev_bd=dev_bd;
	
	//No copying of g, change cells struct to point at *global_dev_g.
	cells->dev_g=global_dev_g;
	
	//Copy Cells over to Device
	gpuErrorCheck(cudaMemcpy(dev_cells, cells, sizeof(Cells),
		cudaMemcpyHostToDevice));
}

void copyOutputGToDevice (float **g, int nx, int nz)
/*****************************************************************************
Copys the output g array to the device. This assume memory has already been
allocated on the device using allocGonDevice(), and the device pointer is
stored in gloab_dev_g.

float **g		array[nx][nz] containing g(x,z)
int nx			number of x samples
int nz			number of z samples
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	if (global_dev_g == NULL) {
		warn("Gloabl_dev_g value is NULL!");
		return;
	}
	
	gpuErrorCheck(cudaMemcpy(global_dev_g, g[0], nx * nz * sizeof(float),
		cudaMemcpyHostToDevice));
}

void copyCellsDeviceToHost (Cells *cells)
/*****************************************************************************
Copys the data pointed to by **g, this is the onyl data that is modified.

Note: The *ray data is not copied as it is not yet used by any written method
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	float *dev_g = cells->dev_g, **g = cells->g;
	int nx = cells->nx, nz = cells->nz;

	gpuErrorCheck(cudaMemcpy(g[0], dev_g, nx * nz * sizeof(float),
		cudaMemcpyDeviceToHost));
}

void copyOutputGToHost (float **g, int nx, int nz)
/*****************************************************************************
Copys the data from the device using global_dev_g back to the host location
of **g

float **g		array[nx][nz] containing g(x,z)
int nx			number of x samples
int nz			number of z sample
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	if (global_dev_g == NULL) {
		warn("Gloabl_dev_g value is NULL!");
		return;
	}
	
	gpuErrorCheck(cudaMemcpy(g[0], global_dev_g, nx * nz * sizeof(float),
		cudaMemcpyDeviceToHost));
}

void checkCells (Cells *cells, Cells *dev_cells)
/*****************************************************************************
Checks the contents of the Cells struct on the device
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	int nt, lx, mx, nz, live, dead;
	float dt, ft, dx, fx, dz, fz, wmin, lmin;
	
	int *dev_nt, *dev_lx, *dev_mx, *dev_nz, *dev_live, *dev_dead;
	float *dev_dt, *dev_ft, *dev_dx, *dev_fx, *dev_dz, *dev_fz, *dev_wmin,
		 *dev_lmin;
	bool correct=false;

	cudaMalloc((void**) &dev_nt, sizeof(int));
	cudaMalloc((void**) &dev_lx, sizeof(int));
	cudaMalloc((void**) &dev_mx, sizeof(int));
	cudaMalloc((void**) &dev_nz, sizeof(int));
	cudaMalloc((void**) &dev_live, sizeof(int));
	cudaMalloc((void**) &dev_dead, sizeof(int));
	
	cudaMalloc((void**) &dev_dt, sizeof(float));
	cudaMalloc((void**) &dev_ft, sizeof(float));
	cudaMalloc((void**) &dev_dx, sizeof(float));
	cudaMalloc((void**) &dev_fx, sizeof(float));
	cudaMalloc((void**) &dev_dz, sizeof(float));
	cudaMalloc((void**) &dev_fz, sizeof(float));
	cudaMalloc((void**) &dev_wmin, sizeof(float));
	cudaMalloc((void**) &dev_lmin, sizeof(float));

	devCheckCells<<<1,1>>>(dev_cells, dev_nt, dev_lx, dev_mx, dev_nz,
		 dev_live, dev_dead, dev_dt, dev_ft, dev_dx, dev_fx, dev_dz,
		  dev_fz, dev_wmin, dev_lmin);
	gpuErrorCheck(cudaPeekAtLastError());

	cudaMemcpy(&nt, dev_nt, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&lx, dev_lx, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&mx, dev_mx, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&nz, dev_nz, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&live, dev_live, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&dead, dev_dead, sizeof(int), cudaMemcpyDeviceToHost);
	
	cudaMemcpy(&dt, dev_dt, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&ft, dev_ft, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&dx, dev_dx, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&fx, dev_fx, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&dz, dev_dz, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&fz, dev_fz, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&wmin, dev_wmin, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&lmin, dev_lmin, sizeof(float), cudaMemcpyDeviceToHost);

	if (nt != cells->nt){ warn("nt is not the same on device %d and host %d", nt, cells->nt); correct=true;}
	if (lx != cells->lx){ warn("lx is not the same on device %d and host %d", lx, cells->lx); correct=true;}
	if (mx != cells->mx){ warn("mx is not the same on device %d and host %d", mx, cells->mx); correct=true;}
	if (nz != cells->nz){ warn("nz is not the same on device %d and host %d", nz, cells->nz); correct=true;}
	if (live != cells->live){ warn("live is not the same on device %d and host %d", live, cells->live); correct=true;}
	if (dead != cells->dead){ warn("dead is not the same on device %d and host %d", dead, cells->dead); correct=true;}
	
	if (dt != cells->dt){ warn("dt is not the same on device %f and host %f", dt, cells->dt); correct=true;}
	if (ft != cells->ft){ warn("ft is not the same on device %f and host %f", ft, cells->ft); correct=true;}
	if (dx != cells->dx){ warn("dx is not the same on device %f and host %f", dx, cells->dx); correct=true;}
	if (fx != cells->fx){ warn("fx is not the same on device %f and host %f", fx, cells->fx); correct=true;}
	if (dz != cells->dz){ warn("dz is not the same on device %f and host %f", dz, cells->dz); correct=true;}
	if (fz != cells->fz){ warn("fz is not the same on device %f and host %f", fz, cells->fz); correct=true;}
	if (wmin != cells->wmin){ warn("wmin is not the same on device %f and host %f", wmin, cells->wmin); correct=true;}
	if (lmin != cells->lmin){ warn("lmin is not the same on device %f and host %f", lmin, cells->lmin); correct=true;}
	if (!correct) warn("Data on Device matches Host");
	cudaFree(dev_nt);
	cudaFree(dev_lx);
	cudaFree(dev_mx);
	cudaFree(dev_nz);
	cudaFree(dev_live);
	cudaFree(dev_dead);
	
	cudaFree(dev_dt);
	cudaFree(dev_ft);
	cudaFree(dev_dx);
	cudaFree(dev_fx);
	cudaFree(dev_dz);
	cudaFree(dev_fz);
	cudaFree(dev_wmin);
	cudaFree(dev_lmin);
}

__global__ static void devCheckCells(Cells *cells,
 int *nt, int *lx, int *mx, int *nz, int *live, int *dead,
 float *dt, float *ft, float *dx, float *fx, float *dz, float *fz,
  float *wmin, float *lmin) {
	*nt=cells->nt;
	*lx=cells->lx;
	*mx=cells->mx;
	*nz=cells->nz;
	*live=cells->live;
	*dead=cells->dead;
	
	*dt=cells->dt;
	*ft=cells->ft;
	*dx=cells->dx;
	*fx=cells->fx;
	*dz=cells->dz;
	*fz=cells->fz;
	*wmin=cells->wmin;
	*lmin=cells->lmin;
}
