#include "gpu.h"

/*****************************************************************************
Origionaly method from sum_main_gpu

static void accCell (Cells *cells, Cells *dev_cells, int jx, int jz)
******************************************************************************
Accumulate the contribution of a Gaussian beam in a cell and its
neighbors, recursively.
******************************************************************************
Input:
cells		pointer to cells
jx		x index of the cell to fill
jz		z index of the cell to fill
******************************************************************************
Notes:
To reduce the amount of memory required for recursion, the actual
accumulation is performed by function cellBeam(), so that no local
variables are required in this function, except for the input
arguments themselves.
******************************************************************************
{
	//if cell is out of bounds, return 
	if (jx<0 || jx>=cells->mx-1 || jz<0 || jz>=cells->mz-1) return;

	//if cell is dead, return
	if (cells->cell[jx][jz].dead==cells->dead) return;

	//make cell dead
	cells->cell[jx][jz].dead = cells->dead;

	//if upper-left corner of cell is not live, return
	if (cells->cell[jx][jz].live!=cells->live) return;

	//if remaining three corners of cell are live
	if (cells->cell[jx+1][jz].live==cells->live &&
		cells->cell[jx][jz+1].live==cells->live &&
		cells->cell[jx+1][jz+1].live==cells->live) {

		//accumulate beam in cell
		clock_t t3 = clock();
		cellBeam(cells, dev_cells, jx,jz);
		overallCellBeam += (clock()-t3)/(double)(CLOCKS_PER_SEC);
	}

	//recursively accumulate in neighboring cells
	accCell(cells,dev_cells,jx+1,jz);
	accCell(cells,dev_cells,jx-1,jz);
	accCell(cells,dev_cells,jx,jz+1);
	accCell(cells,dev_cells,jx,jz-1);
}
*****************************************************************************/

/****************************Used Internally*********************************/
__global__ void accumulate(Cells *dev_cells);
__global__ void accumulateIndividual (Cells *dev_cels);
__device__ void cellBeam_dev_Imp4 (Cells *cells, int jx, int jz);
__device__ void cellBeam_dev_Imp5 (Cells *cells, int jx, int jz);

void accumulateCellDevice (Cells *cells)
/*****************************************************************************
Enterance point for the accumulation of cells. This function no longer uses
recursion instead leaveraing the gpu to check an accumulate the beam in all
cells simultaniously.
This function wil be called once per beam and also handels all of the memory
managment (as opposed to other implementation where the memory managment is
handeled in the parent method).

No copying of data back to the host is done here as this is handeled at a
higher level to reduce the numbeer of MemCpys

*cells			host cells struct
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Allocate and cpy cells host to device
	Cells *dev_cells = allocGpuCells();
	copyCellsHostToDeviceNoG (cells, dev_cells);
	
	//Implementation 4 using (block, thread) for cell index
	accumulate<<<cells->mx, cells->mz>>>(dev_cells);
	//gpuErrorCheck(cudaPeekAtLastError());
	
	//Implementation 5 using (block(GRID), thread(GRID) for output g index
	//dim3 blocks(cells->mx, (cells->mz + 1)/ 2);
	//dim3 threads(cells->lx * 2, cells->lz);
	//accumulateIndividual<<<blocks, threads>>>(dev_cells);
	//gpuErrorCheck(cudaPeekAtLastError());
	
	//Free cells from device
	freeGpuCellsNoG (cells, dev_cells);
}

__global__ void accumulate(Cells *dev_cells)
/*****************************************************************************
Chekcs several stored parameters to acertain wether or not the data within
the cell is worth accumulating.

*dev_cells		device cells struct
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	int jx = blockIdx.x, jz = threadIdx.x;
	int mx = dev_cells->mx, mz = dev_cells->mz;
	Cell *dev_cell = dev_cells->dev_cell;
	
	
	//if cell is out of bounds return
	if (jx>=mx-1 || jz>=mz-1) return;
	
	//if cell is dead, return
	if (dev_cell[jx * mz + jz].dead==dev_cells->dead) return;
	
	//if upper-left corner of cell is not live, return
	if (dev_cell[jx * mz + jz].live!=dev_cells->live) return;

	//Accumulate beam within cell
	cellBeam_dev_Imp4 (dev_cells, jx, jz);
	
}

__global__ void accumulateIndividual(Cells *dev_cells)
/*****************************************************************************
Chekcs several stored parameters to acertain wether or not the data within
the cell is worth accumulating.

Note: This method differes from above by calling a different internal function

*dev_cells		device cells struct
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	int jx = blockIdx.x, jz = threadIdx.x > 7 ? blockIdx.y * 2 + 1 : blockIdx.y * 2;
	int mx = dev_cells->mx, mz = dev_cells->mz;
	Cell *dev_cell = dev_cells->dev_cell;
	
	//if cell is out of bounds return
	if (jx>=mx-1 || jz>=mz-1) return;
	
	//if cell is dead, return
	if (dev_cell[jx * mz + jz].dead==dev_cells->dead) return;
	
	//if upper-left corner of cell is not live, return
	if (dev_cell[jx * mz + jz].live!=dev_cells->live) return;

	//Accumulate beam within cell
	cellBeam_dev_Imp5 (dev_cells, jx, jz);
}

__device__ void cellBeam_dev_Imp4 (Cells *cells, int jx, int jz)
/*****************************************************************************
Accumulate Gaussian beam for one cell, on device.

*cells		device cells struct
jx		x index of the cell to fill
jz		z index of the cell to fill		
******************************************************************************
Input:
cells		pointer to cells
jx		x index of the cell in which to accumulate beam
jz		z index of the cell in which to accumulate beam
*****************************************************************************/
{
	int mz=cells->mz;
	float *g=cells->dev_g;
	Cell *cell=cells->dev_cell;
	BeamData *bd=cells->dev_bd;
	int ntr=bd->ntr,nti=bd->nti;
	float dtr=bd->dtr,ftr=bd->ftr,dti=bd->dti,fti=bd->fti;
	complex *cf=bd->dev_cf;
	int kxlo,kxhi,kzlo,kzhi,kx,kz,itr,iti;
	float odtr,odti,t00r,t01r,t10r,t11r,t00i,t01i,t10i,t11i,
		a00r,a01r,a10r,a11r,a00i,a01i,a10i,a11i,
		tx0r,tx1r,tx0i,tx1i,ax0r,ax1r,ax0i,ax1i,
		txzr,txzi,axzr,axzi,
		xdelta,zdelta,
		trn,tin,trfrac,mtrfrac,tifrac,mtifrac,
		cfr,cfi;
	complex *cf0,*cf1;

	//inverse of time sampling intervals
	odtr = 1.0/dtr;
	odti = 1.0/dti;

	// complex time and amplitude for each corner
	t00r = cell[jx  * mz + jz].tr;
	t01r = cell[jx * mz + (jz+1)].tr;
	t10r = cell[(jx+1)* mz + jz ].tr;
	t11r = cell[(jx+1) * mz + (jz+1)].tr;
	t00i = cell[jx * mz + jz].ti;
	t01i = cell[jx * mz + (jz+1)].ti;
	t10i = cell[(jx+1) * mz + jz].ti;
	t11i = cell[(jx+1) * mz + (jz+1)].ti;
	a00r = cell[jx * mz + jz].ar;
	a01r = cell[jx * mz + (jz+1)].ar;
	a10r = cell[(jx+1) * mz + (jz)].ar;
	a11r = cell[(jx+1) * mz + (jz+1)].ar;
	a00i = cell[jx * mz + jz].ai;
	a01i = cell[jx * mz + (jz+1)].ai;
	a10i = cell[(jx+1) * mz + jz].ai;
	a11i = cell[(jx+1) * mz + (jz+1)].ai;

	// x and z samples for cell
	kxlo = jx*cells->lx;
	kxhi = kxlo+cells->lx;
	if (kxhi>cells->nx) kxhi = cells->nx;
	kzlo = jz*cells->lz;
	kzhi = kzlo+cells->lz;
	if (kzhi>cells->nz) kzhi = cells->nz;

	// fractional increments for linear interpolation
	xdelta = 1.0/cells->lx;
	zdelta = 1.0/cells->lz;

	/* increments for times and amplitudes at top and bottom of cell
	dtx0r = (t10r-t00r)*xdelta;
	dtx1r = (t11r-t01r)*xdelta;
	dtx0i = (t10i-t00i)*xdelta;
	dtx1i = (t11i-t01i)*xdelta;
	dax0r = (a10r-a00r)*xdelta;
	dax1r = (a11r-a01r)*xdelta;
	dax0i = (a10i-a00i)*xdelta;
	dax1i = (a11i-a01i)*xdelta;*/
	
	// times and amplitudes at top-left and bottom-left of cell
	tx0r = t00r;
	tx1r = t01r;
	tx0i = t00i;
	tx1i = t01i;
	ax0r = a00r;
	ax1r = a01r;
	ax0i = a00i;
	ax1i = a01i;

	// loop over x samples
	for (kx=kxlo; kx<kxhi; ++kx) {
		/* increments for time and amplitude
		dtxzr = (tx1r-tx0r)*zdelta;
		dtxzi = (tx1i-tx0i)*zdelta;
		daxzr = (ax1r-ax0r)*zdelta;
		daxzi = (ax1i-ax0i)*zdelta;*/

		// time and amplitude at top of cell
		txzr = tx0r;
		txzi = tx0i;
		axzr = ax0r;
		axzi = ax0i;

		// loop over z samples
		for (kz=kzlo; kz<kzhi; ++kz) {
			// index of imaginary time
			iti = tin = (txzi-fti)*odti;
			if (iti<0 || iti>=nti-1) continue;

			// pointers to left and right imaginary time samples
			cf0 = &cf[iti * nti];
			cf1 = &cf[iti+1 * nti];

			// imaginary time linear interpolation coefficients
			tifrac = tin-iti;
			mtifrac = 1.0-tifrac;

			// index of real time
			itr = trn = (txzr-ftr)*odtr;
			if (itr<0 || itr>=ntr-1) continue;

			// real time linear interpolation coefficients
			trfrac = trn-itr;
			mtrfrac = 1.0-trfrac;

			/* real and imaginary parts of complex beam data
			cf0r = mtrfrac*cf0[itr].r+trfrac*cf0[itr+1].r;
			cf1r = mtrfrac*cf1[itr].r+trfrac*cf1[itr+1].r;
			cfr = mtifrac*cf0r+tifrac*cf1r;
			cf0i = mtrfrac*cf0[itr].i+trfrac*cf0[itr+1].i;
			cf1i = mtrfrac*cf1[itr].i+trfrac*cf1[itr+1].i;
			cfi = mtifrac*cf0i+tifrac*cf1i;*/

			cfr = mtifrac*(mtrfrac*cf0[itr].r+trfrac*cf0[itr+1].r)+tifrac*(mtrfrac*cf1[itr].r+trfrac*cf1[itr+1].r);
			cfi = mtifrac*(mtrfrac*cf0[itr].i+trfrac*cf0[itr+1].i)+tifrac*(mtrfrac*cf1[itr].i+trfrac*cf1[itr+1].i);
			
			// accumulate beam
			g[kx * cells->nz + kz] += axzr*cfr-axzi*cfi;

			// increment time and amplitude
			txzr += (tx1r-tx0r)*zdelta;
			txzi += (tx1i-tx0i)*zdelta;
			axzr += (ax1r-ax0r)*zdelta;
			axzi += (ax1i-ax0i)*zdelta;
		}

		// increment times and amplitudes at top and bottom of cell
		tx0r += (t10r-t00r)*xdelta;
		tx1r += (t11r-t01r)*xdelta;
		tx0i += (t10i-t00i)*xdelta;
		tx1i += (t11i-t01i)*xdelta;
		ax0r += (a10r-a00r)*xdelta;
		ax1r += (a11r-a01r)*xdelta;
		ax0i += (a10i-a00i)*xdelta;
		ax1i += (a11i-a01i)*xdelta;
	}
}

__device__ void cellBeam_dev_Imp5 (Cells *cells, int jx, int jz)
/*****************************************************************************
Performs beam accumulation on one point within a cell defined by its internal
cell x, y cordinate. each thread withing a grid will perform for one point.

*cells		cells on device
jx		x index of the cell
jz		z index of the cell
******************************************************************************
Author: Adam Phillips
*****************************************************************************/
{
	//Setup of initial parameters with ** changed to *
	int lx=cells->lx,lz=cells->lz,nx=cells->nx,nz=cells->nz,mz=cells->mz;
	float *g=cells->dev_g;
	Cell *cell=cells->dev_cell;
	BeamData *bd=cells->dev_bd;
	int ntr=bd->ntr,nti=bd->nti;
	float dtr=bd->dtr,ftr=bd->ftr,dti=bd->dti,fti=bd->fti;
	complex *cf=bd->dev_cf;
	int kxlo,kxhi,kzlo,kzhi,kx,kz,itr,iti;
	float odtr,odti,t00r,t01r,t10r,t11r,t00i,t01i,t10i,t11i,
		a00r,a01r,a10r,a11r,a00i,a01i,a10i,a11i,
		tx0r,tx1r,tx0i,tx1i,ax0r,ax1r,ax0i,ax1i,
		txzr,txzi,axzr,axzi,
		dtx0r,dtx0i,dtx1r,dtx1i,dax0r,dax0i,dax1r,dax1i,
		dtxzr,dtxzi,daxzr,daxzi,xdelta,zdelta,
		trn,tin,trfrac,mtrfrac,tifrac,mtifrac,
		cf0r,cf0i,cf1r,cf1i,cfr,cfi;
	complex *cf0,*cf1;
	int xid, zid;

	//inverse of time sampling intervals
	odtr = 1.0/dtr;
	odti = 1.0/dti;

	// complex time and amplitude for each corner
	t00r = cell[jx  * mz + jz].tr;
	t01r = cell[jx * mz + (jz+1)].tr;
	t10r = cell[(jx+1)* mz + jz ].tr;
	t11r = cell[(jx+1) * mz + (jz+1)].tr;
	t00i = cell[jx * mz + jz].ti;
	t01i = cell[jx * mz + (jz+1)].ti;
	t10i = cell[(jx+1) * mz + jz].ti;
	t11i = cell[(jx+1) * mz + (jz+1)].ti;
	a00r = cell[jx * mz + jz].ar;
	a01r = cell[jx * mz + (jz+1)].ar;
	a10r = cell[(jx+1) * mz + (jz)].ar;
	a11r = cell[(jx+1) * mz + (jz+1)].ar;
	a00i = cell[jx * mz + jz].ai;
	a01i = cell[jx * mz + (jz+1)].ai;
	a10i = cell[(jx+1) * mz + jz].ai;
	a11i = cell[(jx+1) * mz + (jz+1)].ai;

	// x and z samples for cell
	kxlo = jx*lx;
	kxhi = kxlo+lx;
	if (kxhi>nx) kxhi = nx;
	kzlo = jz*lz;
	kzhi = kzlo+lz;
	if (kzhi>nz) kzhi = nz;

	// fractional increments for linear interpolation
	xdelta = 1.0/lx;
	zdelta = 1.0/lz;

	// increments for times and amplitudes at top and bottom of cell
	dtx0r = (t10r-t00r)*xdelta;
	dtx1r = (t11r-t01r)*xdelta;
	dtx0i = (t10i-t00i)*xdelta;
	dtx1i = (t11i-t01i)*xdelta;
	dax0r = (a10r-a00r)*xdelta;
	dax1r = (a11r-a01r)*xdelta;
	dax0i = (a10i-a00i)*xdelta;
	dax1i = (a11i-a01i)*xdelta;
	
	// times and amplitudes at top-left and bottom-left of cell
	tx0r = t00r;
	tx1r = t01r;
	tx0i = t00i;
	tx1i = t01i;
	ax0r = a00r;
	ax1r = a01r;
	ax0i = a00i;
	ax1i = a01i;
	
	//Check bounds of block/thread id are within bounds
	xid = threadIdx.x % 8;
	zid = threadIdx.y;
	if(xid < lx && zid < lz) {
		//Output array id, now only one after conversion
		kx = kxlo + xid;
		kz = kzlo + zid;
		
		// increment times and amplitudes at top and bottom of cell
		//tx0r += dtx0r;
		tx0r = tx0r + (xid * dtx0r);
		//tx1r += dtx1r;
		tx1r = tx1r + (xid * dtx1r);
		//tx0i += dtx0i;
		tx0i = tx0i + (xid * dtx0i);
		//tx1i += dtx1i;
		tx1i = tx1i + (xid * dtx1i);
		//ax0r += dax0r;
		ax0r = ax0r + (xid * dax0r);
		//ax1r += dax1r;
		ax1r = ax1r + (xid * dax1r);
		//ax0i += dax0i;
		ax0i = ax0i + (xid * dax0i);
		//ax1i += dax1i;
		ax1i = ax1i + (xid * dax1i);
		
		// increments for time and amplitude
		dtxzr = (tx1r-tx0r)*zdelta;
		dtxzi = (tx1i-tx0i)*zdelta;
		daxzr = (ax1r-ax0r)*zdelta;
		daxzi = (ax1i-ax0i)*zdelta;
		
		// time and amplitude at top of cell
		//txzr = tx0r;
		txzr = tx0r + (zid * dtxzr);
		//txzi = tx0i;
		txzi = tx0i + (zid * dtxzi);
		//axzr = ax0r;
		axzr = ax0r + (zid * daxzr);
		//axzi = ax0i;
		axzi = ax0i + (zid * daxzi);
		
		//Same core computation now done on each thread
		// index of imaginary time
		iti = tin = (txzi-fti)*odti;
		if (iti<0 || iti>=nti-1) return;

		// pointers to left and right imaginary time samples
		cf0 = &cf[iti * nti];
		cf1 = &cf[iti+1 * nti];

		// imaginary time linear interpolation coefficients
		tifrac = tin-iti;
		mtifrac = 1.0-tifrac;

		// index of real time
		itr = trn = (txzr-ftr)*odtr;
		if (itr<0 || itr>=ntr-1) return;

		// real time linear interpolation coefficients
		trfrac = trn-itr;
		mtrfrac = 1.0-trfrac;

		// real and imaginary parts of complex beam data
		cf0r = mtrfrac*cf0[itr].r+trfrac*cf0[itr+1].r;
		cf1r = mtrfrac*cf1[itr].r+trfrac*cf1[itr+1].r;
		cfr = mtifrac*cf0r+tifrac*cf1r;
		cf0i = mtrfrac*cf0[itr].i+trfrac*cf0[itr+1].i;
		cf1i = mtrfrac*cf1[itr].i+trfrac*cf1[itr+1].i;
		cfi = mtifrac*cf0i+tifrac*cf1i;

		// accumulate beam
		g[kz + kx * nz] += axzr*cfr-axzi*cfi;
	}
}
