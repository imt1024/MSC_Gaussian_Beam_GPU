/* Copyright (c) Colorado School of Mines, 2011.*/
/* All rights reserved.                       */

/* SUMIGGBZO: $Revision: 1.15 $ ; $Date: 2011/11/16 22:14:43 $		*/

#include "su.h"
#include "segy.h"

/*********************** self documentation **********************/
char *sdoc[] = {
"									",
" SUMIGGBZO - MIGration via Gaussian Beams of Zero-Offset SU data	",
"									",
" sumiggbzo <infile >outfile vfile=  nz= [optional parameters]		",
"									",
" Required Parameters:							",
" vfile=                 name of file containing v(z,x)			",
" nz=                    number of depth samples			",
"									",
" Optional Parameters:							",
" dt=from header		time sampling interval			",
" dx=from header(d2) or 1.0	spatial sampling interval 		",
" dz=1.0                 depth sampling interval			",
" fmin=0.025/dt          minimum frequency				",
" fmax=10*fmin           maximum frequency				",
" amin=-amax             minimum emergence angle; must be > -90 degrees	",
" amax=60                maximum emergence angle; must be < 90 degrees	",
" bwh=0.5*vavg/fmin      beam half-width; vavg denotes average velocity	",
" verbose=0		 =0 silent; =1 chatty				",
"									",
" Note: spatial units of v(z,x) must be the same as those of dx.	",
" v(z,x) is represented numerically in C-style binary floats v[x][z],	",
" where the depth direction is the fast direction in the data. Such	",
" models can be created with unif2 or makevel.				",
"									",
"(In C  v[iz][ix] denotes a v(x,z) array, whereas v[ix][iz]  		",
" denotes a v(z,x) array, the opposite of what Matlab and Fortran	",
" programmers may expect.)						", 
"									",
" Caveat:								",
" In the event of a \"Segmentation Violation\" try reducing the value of",
" the \"bwh\" parameter. Run program with verbose=1 do see the default	",
" value.								",
NULL};

/* Credits:
 *
 * CWP: Dave Hale (algorithm), Jack K. Cohen, and John Stockwell
 * (reformatting for SU)
 */

/**************** end self doc ***********************************/

/* functions defined and used internally */
void miggbzo (float bwh, float fmin, float fmax, float amin, float amax,
	int nt, float dt, int nx, float dx, int nz, float dz, 
	float **f, float **v, float **g, int verbose);

segy tr;

int main(int argc, char **argv)
{
	int nx;		/* number of horizontal samples (traces) in input */
	int nz;		/* number of vertical samples in output	          */	
	int nt;		/* number of time samples in input		  */
	int ix;		/* counter in x					  */
	int iz;		/* counter in z					  */

	float dx;	/* trace spacing in input			  */
	float dz;	/* vertical sampling interval in output		  */
	float dt;	/* time sampling interval in input	          */

	float fmin;	/* minimum frequency in migration		  */
	float fmax;     /* maximum frequency in migration 		  */
	float amin;	/* minimum ray angle				  */
	float amax;     /* maximum ray angle 			          */
	float vavg;	/* average velocity in input vfile		  */

	float bwh;	/* gaussian beam half-width			  */

	float **v=NULL;	/* pointer to background velocities		  */
	float **f=NULL; /* pointer to input data                          */
	float **g=NULL; /* pointer to migrated data 			  */

	char *vfile="";		/* velocity filename		*/
	FILE *vfp;		/* velocity file pointer	*/
	FILE *tracefp;		/* temp file to hold traces	*/
	int verbose;		/* verbose flag			*/

	/* hook up getpar */
	initargs(argc,argv);
	requestdoc(0);


	/* get info from first trace */ 
	if (!gettr(&tr))  err("can't get first trace");
	nt = tr.ns;

	/* get required parameters */
	MUSTGETPARSTRING("vfile", &vfile);
	MUSTGETPARINT("nz", &nz);
	MUSTGETPARFLOAT("dz", &dz);
	if (!getparint("verbose",&verbose))	verbose = 0;


	/* let user give dt and/or dx from command line */
	if (!getparfloat("dt", &dt)) {
		if (tr.dt) { /* is dt field set? */
			dt = (float) tr.dt / 1000000.0;
		} else { /* dt not set, assume 4 ms */
			dt = 0.004;
			if (verbose) warn("tr.dt not set, assuming dt=0.004");
		}
	}
	if (!getparfloat("dx",&dx)) {
		if (tr.d2) { /* is d2 field set? */
			dx = tr.d2;
		} else {
			dx = 1.0;
			if (verbose) warn("tr.d2 not set, assuming d2=1.0");
		}
	}

	
	/* get optional parameters */
	if (!getparfloat("dz",&dz)) dz = 1.0;
	if (!getparfloat("fmin",&fmin)) fmin = 0.025/dt;
	if (!getparfloat("fmax",&fmax)) fmax = 10.0*fmin;
	if (!getparfloat("amax",&amax)) amax = 60.0;
	if (!getparfloat("amin",&amin)) amin = -amax;

	/* store traces in tmpfile while getting a count */
	tracefp = etmpfile();
	nx = 0;
	do { 
		++nx;
		efwrite(tr.data, FSIZE, nt, tracefp);
	} while (gettr(&tr));


	/* allocate workspace */
	f = ealloc2float(nt,nx);
	v = ealloc2float(nz,nx);
	g = ealloc2float(nz,nx);

	/* load traces into the zero-offset array and close tmpfile */
	rewind(tracefp);
	efread(*f, FSIZE, nt*nx, tracefp);
	efclose(tracefp);

	/* read and halve velocities; determine average */
	vfp = efopen(vfile,"r");
	if (efread(*v, FSIZE, nz*nx, vfp)!=nz*nx)
		err("error reading vfile=%s!\n",vfile);
	for (ix=0,vavg=0.0; ix<nx; ++ix) {
		for (iz=0; iz<nz; ++iz) {
			v[ix][iz] *= 0.5;
			vavg += v[ix][iz];
		}
	}
	vavg /= nx*nz;

	/* get beam half-width */
	if (!getparfloat("bwh",&bwh)) bwh = vavg/fmin;
	
        checkpars();

	if (verbose) warn("bhw=%f",bwh);
	
	/* migrate */
	miggbzo(bwh,fmin,fmax,amin,amax,nt,dt,nx,dx,nz,dz,f,v,g,verbose);
	
	/* set header fields and write output */
	tr.ns = nz;
	tr.trid = TRID_DEPTH;
	tr.d1 = dz;
	tr.d2 = dx;
	
	for (ix=0; ix<nx; ++ix) {
		tr.tracl = ix + 1;
		tr.tracr = ix + 1;
		for (iz=0; iz<nz; ++iz) {
			tr.data[iz] = g[ix][iz];
		}
		puttr(&tr);
	}

	/* free workspace */
	free2float(f);
	free2float(v);
	free2float(g);
	//Inserted to test for changability of code
	
	return(CWP_Exit());
}
